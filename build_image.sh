#!/bin/bash
set -e
CURRENT_DIR=$(dirname $0)
IMAGE_NAME=$("$CURRENT_DIR/get_image_name.sh" + " " + "$@")
VERSION=$("$CURRENT_DIR/get_image_version.sh" + " " + "$@")
DOCKERFILE_PATH="$CURRENT_DIR/../"
NO_CACHE_OPTION=''
while [ "$1" != "" ]; do
    case $1 in
        --no-cache )            NO_CACHE_OPTION='--no-cache'
                                ;;
    esac
    shift
done

echo "Building $IMAGE_NAME for version $VERSION"

EXEC="docker build $NO_CACHE_OPTION --tag $IMAGE_NAME:$VERSION $DOCKERFILE_PATH"
echo ${EXEC}
${EXEC}
