#!/bin/bash
CONTAINER_NAME=""

while [ "$1" != "" ]; do
    case $1 in
        --name )                shift
                                CONTAINER_NAME=$1
                                ;;
    esac
    shift
done

if [ "$CONTAINER_NAME" = '' ]; then
  CONTAINER_NAME=${PWD##*/}
fi

echo ${CONTAINER_NAME}

