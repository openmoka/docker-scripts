#!/bin/bash
CONTAINER_NAME=$("$(dirname $0)/get_container_name.sh" + " " + "$@")
rc=$?; if [[ ${rc} != 0 ]]; then exit ${rc}; fi

EXEC="docker exec -it $CONTAINER_NAME /bin/bash"
echo ${EXEC}
${EXEC}
