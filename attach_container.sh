#!/bin/bash
CONTAINER_NAME=$("$(dirname $0)/get_container_name.sh" + " " + "$@")
rc=$?; if [[ ${rc} != 0 ]]; then exit ${rc}; fi

EXEC="docker attach $CONTAINER_NAME"
echo ${EXEC}
${EXEC}

