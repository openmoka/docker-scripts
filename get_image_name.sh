#!/bin/bash

NAME=''

while [ "$1" != "" ]; do
    case $1 in
        --image-name )          shift
                                NAME=$1
                                ;;
    esac
    shift
done

if [ "$NAME" = '' ]; then
  NAME=${PWD##*/}
fi

echo ${NAME}

