#!/bin/bash
set -e
CURRENT_DIR=$(dirname $0)
CONTAINER_NAME=$("$CURRENT_DIR/get_container_name.sh" + " " + "$@")
CONTAINER_PORT=$("$CURRENT_DIR/get_container_port.sh" + " " + "$@")
IMAGE_VERSION=$("$CURRENT_DIR/get_image_version.sh" + " " + "$@")
IMAGE_NAME=$("$CURRENT_DIR/get_image_name.sh" + " " + "$@")

CODE_PATH=$(pwd)
DAEMON_ARG=''
COMMAND='/bin/bash'
#MOUNT_OPTION="-v $CODE_PATH:/backend"
MOUNT_OPTION=""
LINK_OPTION=""
ENTRY_POINT_OPTION=""

#echo "Mounting path $CODE_PATH"

while [ "$1" != "" ]; do
    case $1 in
#        -v | --version )        shift
#                                VERSION=$1
#                                ;;
        -d | --daemon )         DAEMON_ARG='-d'
                                COMMAND=''
                                ;;
#        --nomount )             MOUNT_OPTION=''
#                                ;;
        --link )                LINK_OPTION=$1
                                ;;
	    --entrypoint )		    ENTRY_POINT_OPTION="--entrypoint"
                                while [[ $2 != --* ]]
                                do
                                shift
                                ENTRY_POINT_OPTION="$ENTRY_POINT_OPTION $1"
                                done
                                COMMAND=''
                                ;;
        -h | --help )           usage
                                exit
    esac
    shift
done

if [ "$IMAGE_VERSION" = '' ]; then
  IMAGE_VERSION=$(get_image_version.sh)
fi

if [ "$IMAGE_VERSION" = '' ]; then
  echo "No version was specified. Either specify a version manually (ie. './build_server 1.10') or select a tagged commit to use that tag as version."
  exit 1;
fi

#DAEMON_ARG=''
#COMMAND='/bin/bash'
if [ "$DAEMON_ARG" = '-d' ]; then
  echo "Running image in daemon mode"
else
  echo "Run script with '-d' option to run image in daemon mode"
fi

echo "Running $IMAGE_NAME for version $IMAGE_VERSION"

if [ "$(docker ps -a | grep ${CONTAINER_NAME})" != "" ]; then
    docker stop ${CONTAINER_NAME}
    docker rm ${CONTAINER_NAME}
fi

EXEC="docker run \
    -it \
    $LINK_OPTION \
    $DAEMON_ARG \
    --name $CONTAINER_NAME \
    $MOUNT_OPTION \
    -p 0.0.0.0:$CONTAINER_PORT:80 \
    $ENTRY_POINT_OPTION \
    $IMAGE_NAME:$IMAGE_VERSION \
    $COMMAND"

echo ${EXEC}
${EXEC}
