#!/bin/bash
PORT=""

while [ "$1" != "" ]; do
    case $1 in
        -p | --port )           shift
                                PORT=$1
                                ;;
    esac
    shift
done

if [ "$PORT" = '' ]; then
  PORT=80
fi

echo ${PORT}