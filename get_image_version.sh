#!/bin/bash
VERSION=""

while [ "$1" != "" ]; do
    case $1 in
        -v | --version )        shift
                                VERSION=$1
                                ;;
    esac
    shift
done


if [ "$VERSION" = '' ]; then
VERSION=$(git describe)
if [ "$VERSION" = '' ]; then
    VERSION=$(git describe --tags --abbrev=0)
    if [ "$VERSION" = '' ]; then
      VERSION='1.0'
    else
      VERSION=$(awk -vx=${VERSION} 'BEGIN{ x1=int(x); x2=((x-x1)*100)+1; print x1 "." x2}')
    fi
  fi
fi

if [ "$VERSION" = '' ]; then
  (>&2 echo "You must specify a version with the -v or --version param")
  exit 1;
fi

echo ${VERSION}
